import { Button, Table } from 'antd';
import { NextComponentType } from 'next';
import { useRouter } from 'next/router';
import React, { useContext } from 'react';
import { AuthenticationContext } from '../../frontend/context/authenticationContext';

const Index: NextComponentType = () => {
  const authenticationContext = useContext(AuthenticationContext);
  const router = useRouter();
  console.log(authenticationContext.authenticated);

  return (
    <div style={{ color: 'green' }}>
      here ya go: {router.query.cheese}
      <Button type="primary">hi</Button>
      <Table />
    </div>
  );
};

export default Index;
