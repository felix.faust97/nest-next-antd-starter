import App from 'next/app';
import React from 'react';
import AuthenticationContextProvider from '../frontend/context/authenticationContext';

class MyApp extends App {

  public render() {
    const {Component, pageProps} = this.props;
    return (
      <AuthenticationContextProvider value={{authenticated: true}}>
        <Component {...pageProps} />
      </AuthenticationContextProvider>
    );
  }
}

export default MyApp;
