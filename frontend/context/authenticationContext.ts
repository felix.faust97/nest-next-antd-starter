import React from 'react';

interface AuthenticationContext {
  authenticated: boolean;
}

console.log('Creating context');

export const AuthenticationContext = React.createContext<AuthenticationContext>({
  authenticated: false,
});

const AuthenticationContextProvider = AuthenticationContext.Provider;

export { AuthenticationContextProvider };
export default AuthenticationContextProvider;
