import { Module } from '@nestjs/common';
import { RenderModule } from 'nest-next';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from './config/config.module';
import { ViewsModule } from './views/views.module';

@Module({
  imports: [RenderModule, ConfigModule, ViewsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
}
